# Kube 101

**Kubernetes for Beginers**

An Jupyter Notebook driven slideshow with small demo

View the presentation with: `jupyter nbconvert *.ipynb --to slides --post serve`
